module.exports = {
  purge: {
    enabled: true,
    content: ['./src/*.html'],
  },
  darkMode: false, // or 'media' or 'class'
  theme: {
    extend: {
      colors: {
        dies: {
          gris1: '#EEF0F5',
          gris2: '#000',
          azul1: '#343D5B',
          azul2: '#1A1F2D',
          azul3: '#6978A9',
          verde1: '#299657',
          verde2: '#81DCA7',
          verde3: '#175531',
        }
      }
    },
    
    
  },
  variants: {
    extend: {},
  },
  plugins: [
    
  ],

}
